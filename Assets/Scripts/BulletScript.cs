using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private float speed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetKeyDown("space"))
        {
            //print("space key was pressed");
            transform.Translate(speed * Time.deltaTime * Vector2.down);
        }*/
        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.Translate(speed * Time.deltaTime * Vector2.down);
        }
    }

    public void Shoot()
    {
        transform.Translate(speed * Time.deltaTime * Vector2.down);
    }
}
