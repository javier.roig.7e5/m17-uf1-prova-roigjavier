using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateScript : MonoBehaviour
{
    private int health;
    // Start is called before the first frame update
    void Start()
    {
        health = 5;
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Dead();
        }
    }

    public void Dead()
    {
        gameObject.SetActive(false);
    }
}
