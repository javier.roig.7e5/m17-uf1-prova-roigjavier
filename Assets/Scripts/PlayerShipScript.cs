using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipScript : MonoBehaviour
{
    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    private BulletScript Shooting;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float translationV = Input.GetAxis("Vertical") * speed;
        float translationH = Input.GetAxis("Horizontal") * speed;

        translationV *= Time.deltaTime;
        translationH *= Time.deltaTime;

        transform.Translate(translationV, 0, 0);
        transform.Translate(0, translationH, 0);

        /*if (Input.GetKeyDown("space"))
        {
            //print("space key was pressed");
            //transform.Translate(speed * Time.deltaTime * Vector2.down);
            Shooting.Shoot();
        }*/

    }
}
