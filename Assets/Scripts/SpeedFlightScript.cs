using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightScript : MonoBehaviour
{
    Vector3 _startingPos;
    Transform _trans;
    private double speed = 5f;
    void Start()
    {
        _trans = GetComponent<Transform>();
        _startingPos = _trans.position;
    }
    void Update()
    {
        _trans.position = new Vector3(_startingPos.x, _startingPos.y + Mathf.PingPong(Time.time, 5), _startingPos.z);
    }
}
